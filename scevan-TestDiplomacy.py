
from io import StringIO
from unittest import main, TestCase

from Diplomacy import Army, diplomacy_solve, start_game, support, fight, diplomacy_print


class TestDiplomacy (TestCase):
    # ----
    # start
    # ----

    def test_start_game(self):
        input_data = ['A', 'Tokyo', 'Hold']
        s = start_game(input_data)
        self.assertEqual("A", s.name)
        self.assertEqual("Tokyo", s.city)
        self.assertEqual("Hold", s.action)

    def test_start_game_2(self):
        input_data = ['B', 'Berlin', 'Move', 'Austin']
        s = start_game(input_data)
        self.assertEqual("B", s.name)
        self.assertEqual("Austin", s.city)
        self.assertEqual("Move", s.action)
        self.assertEqual(None, s.modifier)


    def test_start_game_3(self):
        input_data = ['C', 'London', 'Support', 'D']
        s = start_game(input_data)
        self.assertEqual("C", s.name)
        self.assertEqual("London", s.city)
        self.assertEqual("Support", s.action)
        self.assertEqual("D", s.modifier)
        self.assertEqual(0, s.value)
        self.assertEqual(False, s.dead)



    # -----
    # solve
    # -----
    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
        w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
        w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid B\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
        w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")


if __name__ == "__main__": #pragma: no cover
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
......
----------------------------------------------------------------------
Ran 6 tests in 0.001s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
......
----------------------------------------------------------------------
Ran 6 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          66      1     42      1    100%
TestDiplomacy.py      41      0      0      0    100%
--------------------------------------------------------------
TOTAL                110      1     42      1    100%
"""