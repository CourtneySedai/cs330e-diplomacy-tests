from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve


class TestDiplomacy (TestCase):

    def test_1(self):  
        r = StringIO("A Dallas Hold\nB Waco Move Dallas\nC Atlanta Move Dallas\nD Nashville Support B\nE Austin Support A")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Nashville\nE Austin")
        
    def test_2(self):
        r = StringIO("A Moscow Hold\nB Berlin Move Moscow\nC London Move Moscow\nD Paris Support B")

        w = StringIO("")
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Moscow\nC [dead]\nD Paris")

    def test_3(self):
        r = StringIO("A Moscow Hold\nB Berlin Move Moscow\nC London Support B\nD Paris Move London")

        w = StringIO("")
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]")
    
    def test_4(self):
        r = StringIO("")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "")


if __name__ == "__main__": #pragma: no cover
    main()
    
